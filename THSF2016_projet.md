# THSF 2016
Ceci est un appel à participation à tous les hackers, performeurs, 
makers, libre penseurs, artistes...
La date limite de dépôt des propositions est fixé au 15/03/2016


# À PROPOS DU THSF
La TOULOUSE HACKER SPACE FACTORY est un rendez-vous basé sur la mise en
commun  de connaissances, une rencontre autour de propositions
artistiques, de conférences et d’ateliers. L’objectif est de générer un 
espace critique et expérimental.

L’événement est tourné vers l’utilisation libre des technologies, leurs 
appropriations et les enjeux que cela suscite dans la transformation de 
nos sociétés.
Il est ouvert à tout-e-s, débutant-e-s, expert-e-s ou intéressé-e-s.

C’est un temps de rencontres et d’échanges, dont la formule hybride 
stimule la réflexion et la recherche dans de nombreux domaines, 
artistique, technologique, sociologique, philosophique...

# THSF7
du 19 au 22 mai 2016

2016 sera la 7 e édition de cet événement à Mix’Art Myrys Toulouse.

Un certains nombre de sujets essentiels (loi de 
programmation  militaire, révélations Snowden, logiciels libres, 
open data...) ont été solutionnés d’une manière peu satisfaisante ces 
derniers temps. Un sentiment d’abattement à été assez largement partagé 
cet été dans la communauté hackers, et plus largement encore en cette 
fin d’année 2015.
Alors, que faire ensuite ? Que faire maintenant ?

S'appuyer sur la notion de temporalité et de temps long des idées pour 
penser l'action, nous apparaît efficace quand notre volonté est, qu'au 
delà des personnes, au delà du groupe et de l'événement lui même, ces 
idées continuent d'agir et de se perpétuer malgré les aléas du contexte.

Dans cette édition de THSF, nous allons donc mettre à plat la notion de 
temporalité, qu’elle soit :

* individuelle et biologique (psychologie, quotidien, santé),
* politique (deadline de la dette grecque, agora 500 av JC),
* médiatique (temps de cerveau disponible, vitesse de l'info),
* guerrière (vitesse de l’info, temps réel des drones),
* financière (Trading Haute Vitesse),
* artistique (temps réel, évènement éphémère),
* informatique (0 day),
* physique (la variable temps)

# DATES : du 19 au 22 Mai 2016

# MISE EN PLACE
Nous disposons : d'espaces pour des ateliers, d'une salle de conférence, 
d'une salle de concert, d'une salle à tout faire, de stream video, de 
coins et recoins pour les installations, pour les nerds, d'un parking, 
d'un ou plusieurs bars et d'un tuyau vers les internetz d'environ 8Gb/s.

Le lieu sera ouvert 24h sur 24 pendant les 4 jours de l'évènement pour 
tous les participant(e)s.

# CONTACTS
Nous pensons que la diversité contribue fortement à la richesse de 
l'événement. 

Nous espérons vous voir contribuer, nous voulons un événement porté par 
la communauté pour la communauté sans attentes commerciales.

La date limite de dépôt des propositions est fixé au 15/03/2016

Le THSF vous est proposé par le Tetalab, Tetaneutral & Mix'Art Myrys.

# REVUE DE PRESSE
http://www.makery.info/2015/05/19/les-hackers-a-la-rencontre-du-grand-public-au-thsf-de-toulouse

http://www.liberation.fr/futurs/2015/05/15/a-toulouse-les-hackers-surfent-sur-le-canal-du-midi_1310194
