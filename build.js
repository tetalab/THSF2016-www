var fs = require('fs')
var generator =  require('./lib/sitegenerator')


conf = { file : 'THSF2016_projet.md', 
         outfile: "./site/THSF2016_projet.html",
         template : "./templates/cfp.jade" }

var g = new generator.PageMarkdown(conf)
g.render().then(function (html) {
    fs.writeFileSync(conf.outfile, html, {
      encoding: 'utf8',
      flag: 'w'
    });
  });



